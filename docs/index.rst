==============================================
Flyer Composer
==============================================

.. container:: admonition topic

  **Rearrange PDF pages to print as flyers on one paper**

`Flyer Composer` can be used to prepare one- or two-sided flyers for
printing on one sheet of paper.

Imagine you have designed a flyer in A6 format and want to print it using your
A4 printer. Of course, you want to print four flyers on each sheet. This is
where `Flyer Composer` steps in, creating a PDF which holds your flyer
four times. If you have a second page, `Flyer Composer` can arrange it
the same way - even if the second page is in a separate PDF file.


.. toctree::
   :maxdepth: 1

   Installation
   Usage
   Donate <Donate>
   Changes
   Development

.. include:: _common_definitions.txt
