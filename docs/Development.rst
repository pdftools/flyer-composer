Development
===============================


The source of |flyer-composer| and its siblings is maintained at
`GitLab <https://gitlab.com>`_. Patches and pull-requests
are hearty welcome.

* Please submit bugs and enhancements to the `Issue Tracker
  <https://gitlab.com/pdftools/flyer-composer/issues>`_.

* You may browse the code at the
  `Repository Browser
  <https://gitlab.com/pdftools/flyer-composer>`_.
  Or you may check out the current version by running ::

    git clone https://gitlab.com/pdftools/flyer-composer.git



.. include:: _common_definitions.txt
