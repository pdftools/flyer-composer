# Translations template for flyer-composer.
# Copyright (C) 2022 ORGANIZATION
# This file is distributed under the same license as the flyer-composer
# project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2022.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: flyer-composer 1.0rc3.dev0\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2022-12-10 14:35+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.11.0\n"

#: flyer_composer/cli.py:53
#, python-format
msgid "I don't understand your box specification %r"
msgstr ""

#: flyer_composer/cli.py:57
msgid "Offset not allowed in box definition"
msgstr ""

#: flyer_composer/cli.py:68
#, python-format
msgid "I don't understand your papersize name %r"
msgstr ""

#: flyer_composer/cli.py:71
#, python-format
msgid "Your papersize name %r is not unique, give more chars"
msgstr ""

#: flyer_composer/cli.py:103
msgid "list available media and distance names"
msgstr ""

#: flyer_composer/cli.py:107
msgid "show what would have been done, but do not generate files"
msgstr ""

#: flyer_composer/cli.py:113
msgid "front page of flyer (default: first page)"
msgstr ""

#: flyer_composer/cli.py:116
msgid "back page of flyer (default: no back page)"
msgstr ""

#: flyer_composer/cli.py:119
msgid ""
"use the content area defined by the ArtBox (default: use the area defined"
" by the TrimBox)"
msgstr ""

#: flyer_composer/cli.py:126
msgid "specify the finished flyer size (defaults to media size)"
msgstr ""

#: flyer_composer/cli.py:131
#, python-format
msgid "specify the page size of the output media (default: %s)"
msgstr ""

#: flyer_composer/cli.py:153
#, python-format
msgid "The input-file is either currupt or no PDF at all: %s"
msgstr ""

#: flyer_composer/gui/__init__.py:57
msgid "A4"
msgstr ""

#: flyer_composer/gui/__init__.py:58
msgid "A5 on A4"
msgstr ""

#: flyer_composer/gui/__init__.py:59
msgid "A6 on A4"
msgstr ""

#: flyer_composer/gui/__init__.py:60
msgid "DIN Lang on A4"
msgstr ""

#: flyer_composer/gui/__init__.py:197
msgid "&Open…"
msgstr ""

#: flyer_composer/gui/__init__.py:198
msgid "Open PDF document to use for this page"
msgstr ""

#: flyer_composer/gui/__init__.py:219
msgid "Remove this page"
msgstr ""

#: flyer_composer/gui/__init__.py:237
msgid "Select page:"
msgstr ""

#: flyer_composer/gui/__init__.py:251
msgid "Open document"
msgstr ""

#: flyer_composer/gui/__init__.py:253 flyer_composer/gui/__init__.py:416
msgid "PDF document (*.pdf);;All Files (*)"
msgstr ""

#: flyer_composer/gui/__init__.py:314
msgid "&File"
msgstr ""

#: flyer_composer/gui/__init__.py:315
msgid "&Exit"
msgstr ""

#: flyer_composer/gui/__init__.py:330
msgid "Front page"
msgstr ""

#: flyer_composer/gui/__init__.py:330
msgid "Back page"
msgstr ""

#: flyer_composer/gui/__init__.py:336
#, python-format
msgid ""
"If you find %(APPLICATION_NAME)s useful, please <a "
"href=\"%(DONATION_URL)s\">donate</a>!"
msgstr ""

#: flyer_composer/gui/__init__.py:342
msgid "Select target layout:"
msgstr ""

#: flyer_composer/gui/__init__.py:360
msgid "&Save as…"
msgstr ""

#: flyer_composer/gui/__init__.py:361
msgid "Save flyer as PDF"
msgstr ""

#: flyer_composer/gui/__init__.py:369
msgid "&Donate…"
msgstr ""

#: flyer_composer/gui/__init__.py:370
msgid "Support development of this project"
msgstr ""

#: flyer_composer/gui/__init__.py:383
#, python-format
msgid "Failed to load the file %s"
msgstr ""

#: flyer_composer/gui/__init__.py:409
msgid "-flyer"
msgstr ""

#: flyer_composer/gui/__init__.py:414
msgid "Save document"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:295
msgid "usage: "
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:846
msgid ".__call__() not defined"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:1149
#, python-format
msgid "unknown parser %(parser_name)r (choices: %(choices)s)"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:1209
#, python-format
msgid "argument \"-\" with mode %r"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:1218
#, python-format
msgid "can't open '%(filename)s': %(error)s"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:1427
#, python-format
msgid "cannot merge actions - two groups are named %r"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:1465
msgid "'required' is an invalid argument for positionals"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:1487
#, python-format
msgid ""
"invalid option string %(option)r: must start with a character "
"%(prefix_chars)r"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:1507
#, python-format
msgid "dest= is required for options like %r"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:1524
#, python-format
msgid "invalid conflict_resolution value: %r"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:1544
#, python-format
msgid "conflicting option string: %s"
msgid_plural "conflicting option strings: %s"
msgstr[0] ""
msgstr[1] ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:1608
msgid "mutually exclusive arguments must be optional"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:1671
msgid "positional arguments"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:1672
msgid "optional arguments"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:1687
msgid "show this help message and exit"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:1718
msgid "cannot have multiple subparser arguments"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:1770
#: ../../../../../../usr/lib64/python3.8/argparse.py:2277
#, python-format
msgid "unrecognized arguments: %s"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:1867
#, python-format
msgid "not allowed with argument %s"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:1913
#: ../../../../../../usr/lib64/python3.8/argparse.py:1927
#, python-format
msgid "ignored explicit argument %r"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:2034
#, python-format
msgid "the following arguments are required: %s"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:2049
#, python-format
msgid "one of the arguments %s is required"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:2092
msgid "expected one argument"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:2093
msgid "expected at most one argument"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:2094
msgid "expected at least one argument"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:2098
#, python-format
msgid "expected %s argument"
msgid_plural "expected %s arguments"
msgstr[0] ""
msgstr[1] ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:2156
#, python-format
msgid "ambiguous option: %(option)s could match %(matches)s"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:2220
#, python-format
msgid "unexpected option string: %s"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:2417
#, python-format
msgid "%r is not callable"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:2434
#, python-format
msgid "invalid %(type)s value: %(value)r"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:2445
#, python-format
msgid "invalid choice: %(value)r (choose from %(choices)s)"
msgstr ""

#: ../../../../../../usr/lib64/python3.8/argparse.py:2521
#, python-format
msgid "%(prog)s: error: %(message)s\n"
msgstr ""

